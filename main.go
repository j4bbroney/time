package main

import (
	"Project1/handlers"
	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()

	router.GET("/users", handlers.ShowUsers)
	//router.GET("/user/:userID", UserID)
	//router.GET(/user/:email, UserEmail)
	router.POST("/user", handlers.AddUser)
	router.Run(":8000")

}
